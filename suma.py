from Calculadora import  Calculadora
class Suma:
    def __init__(self, calculadora: Calculadora):
        self.calculadora = calculadora

    def action(self, new_value:float):
        if self.calculadora.valor is None:
            self.calculadora.valor = new_value
        else:
            self.calculadora.valor = self.calculadora.valor + new_value

