from Calculadora import  Calculadora
class Igual:
    def __init__(self, calculadora: Calculadora):
        self.calculadora = calculadora

    def action(self):
        if self.calculadora.valor is None:
            print(0.0)
        else:
            print(self.calculadora.valor)
